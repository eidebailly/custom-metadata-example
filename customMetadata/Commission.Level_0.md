<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Level 0</label>
    <protected>false</protected>
    <values>
        <field>Dollar_Amount__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Monthly_Sales__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Rate__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Test_Data__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
