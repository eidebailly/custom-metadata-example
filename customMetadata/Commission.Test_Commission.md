<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>_Test_DO NOT MODIFY</label>
    <protected>true</protected>
    <values>
        <field>Dollar_Amount__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>Monthly_Sales__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>Rate__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>Test_Data__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
