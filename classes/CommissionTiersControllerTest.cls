/**
 * Test class for Commission Tiers Controller
 * @group test
 */
@isTest
private class CommissionTiersControllerTest {

    /**
     * Method to test init method
     * Verify initialization of state object
     */
    static testMethod void test_getAllCommisions() {
        List<Commission__mdt> lstCommissions = [SELECT   Rate__c,
                                                         Monthly_Sales__c,
                                                         Dollar_Amount__c,
                                                         DeveloperName
                                                FROM     Commission__mdt
                                                ORDER BY Rate__c ASC];
        Test.startTest();
        CommissionTiersController.State objState = CommissionTiersController.init();
        Test.stopTest();

        System.AssertNotEquals(null,objState);
        System.AssertEquals(lstCommissions.size(),objState.lstCommissionMetadata.size());
    }
}