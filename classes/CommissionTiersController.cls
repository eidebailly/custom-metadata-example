/**
 * Controller for the Commission Tiers Display page.
 * @group Controller
 */
public class CommissionTiersController {

    /**
     * Initializes all of the variables needed for the page.
     * @return State            State wrapper class that's passed to the page.
     */
    @RemoteAction
    public static State init() {
        State state = new State();
        state.lstCommissionMetadata = CommissionsUtility.getAllCommisions();
        state.lstCommissionCalculated = new List<CommissionWrapper>();

        for(sObject opp:getWonOpportunitiesMTD()) {
            CommissionWrapper objCommissionWrapper = new CommissionWrapper();                
            objCommissionWrapper.userName =String.valueOf(opp.get('Name'));
            Double soldCount=Double.valueOf(opp.get('Sales'));
            Double soldDollars=Double.valueOf(opp.get('Dollars'));
            objCommissionWrapper.commissionCalculated = CommissionsUtility.FindCommission(soldCount, soldDollars);
            objCommissionWrapper.rate = CommissionsUtility.FindRate(soldCount, soldDollars);
            objCommissionWrapper.totalDollars = soldDollars;
            state.lstCommissionCalculated.add(objCommissionWrapper);
        }
        return state;
    }

    /**
     * Method to query all Opportunity records
     * @return  List<sObject>   lstOpportunities  list of Opportunities records closed won this month
     */
    public static List<sObject> getWonOpportunitiesMTD() {
        List<sObject> lstOpportunities = [SELECT Owner.Name Name, OwnerId, SUM(Amount) Dollars, Count(Id) Sales
                                              FROM   Opportunity
                                              WHERE  CloseDate = THIS_MONTH
                                              AND    IsWon=true
                                              AND    IsClosed=true
         										GROUP BY OwnerID, Owner.Name];
        return lstOpportunities;
    }

    /**
     * Wrapper class
     */
    public class State {
        public List<Commission__mdt> lstCommissionMetadata;
        public List<CommissionWrapper> lstCommissionCalculated;
    }

    /**
     * Wrapper class for commission
     */
    public class CommissionWrapper {
        public String userName;
        public Double totalDollars=0;
        public Double rate=0;
        public Double commissionCalculated;
    }

}