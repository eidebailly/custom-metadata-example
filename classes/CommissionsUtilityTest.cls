/**
 * Test class for Commission Utility
 * @group test
 */
@isTest
private class CommissionsUtilityTest {

    /**
     * Method to test Utility class method 'getAllCommisions'
     */
    static testMethod void test_getAllCommisions() {
        List<Commission__mdt> lstCommissions = [SELECT   Rate__c,
                                                Monthly_Sales__c,
                                                Dollar_Amount__c,
                                                DeveloperName
                                                FROM     Commission__mdt
                                                WHERE (Test_Data__c = :Test.isRunningTest())];
        Test.startTest();
        List<Commission__mdt> lstCommissionsActual = CommissionsUtility.getAllCommisions();
        Test.stopTest();

        // assert that expected list is equal to actual list
        System.AssertEquals(lstCommissions.size(), lstCommissionsActual.size(), 'The returned commission list size is not correct.');
    }

    /**
     * Method to test Utility class method 'FindCommission'
     */
    static testMethod void test_FindCommission() {
        Double salesCount = 10;
        Double dollar = 5000;

        Test.startTest();
        Double commissionsActual = CommissionsUtility.FindCommission(salesCount, dollar);
        Test.stopTest();

        // assert that expected commission is equal to actual commission
        System.AssertEquals(250, commissionsActual, 'The commission rate does not match. Please check to ensure the Test Data has not been altered.');
    }

    static testMethod void test_FindCommissionBelowThreshold() {
        Double salesCount = 1;
        Double dollar = 1;

        Test.startTest();
        Double commissionsActual = CommissionsUtility.FindCommission(salesCount, dollar);
        Test.stopTest();

        // assert that expected commission is equal to actual commission
        System.AssertEquals(0.0, commissionsActual, 'The commission rate does not match. Please check to ensure the Test Data has not been altered.');
    }

    /**
     * Method to test Utility class method 'FindRate'
     */
    static testMethod void test_FindRate() {
        Double salesCount = 10;
        Double dollar = 5000;

        Test.startTest();
        Double rateActual = CommissionsUtility.FindRate(salesCount, dollar);
        Test.stopTest();

        // assert that expected rate is equal to actual rate
        System.AssertEquals(5.0, rateActual, 'The commission rate does not match. Please check to ensure the Test Data has not been altered.');
    }

    /**
     * Method to test Utility class method 'RateBySales'
     */
    static testMethod void test_RateBySales() {
        Double salesCount = 10;

        Test.startTest();
        Double rateBySalesActual = CommissionsUtility.RateBySales(salesCount);
        Test.stopTest();

        // assert that expected is equal to RateBySales
        System.AssertEquals(5.0, rateBySalesActual, 'The commission rate does not match. Please check to ensure the Test Data has not been altered.');
    }

    /**
     * Method to test Utility class method 'RateByDollars'
     */
    static testMethod void test_RateByDollars() {
        Double dollar = 5000;

        Test.startTest();
        Double rateByDollarsActual = CommissionsUtility.RateByDollars(dollar);
        Test.stopTest();

        // assert that expected is equal to RateByDollars
        System.AssertEquals(5.0, rateByDollarsActual, 'The commission rate does not match. Please check to ensure the Test Data has not been altered.');
    }
}