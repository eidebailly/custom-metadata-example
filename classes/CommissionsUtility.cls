/**
 * Utility to find the commission rate for a given sales amount.
 * @group Utility
 */
public with sharing class CommissionsUtility {
    /**
     * Find the Total Dollar commission based on monthly sales and Dollars sold
     * @param  salesCount Number of sales this Month
     * @param  dollars    Dollar value of sales
     * @return Double     The commission amount in dollars.
     */
    public static Double FindCommission(Double salesCount, Double dollars) {
        return dollars * Math.MAX(RateBySales(salesCount), RateByDollars(dollars)) / 100;
    }

    /**
     * Find the Percentage rate for a given number of sales of dollars sold.
     * @param  salesCount Number of sales this Month
     * @param  dollars    Dollar value of sales
     * @return Double     Percentage for the commission
     */
    public static Double FindRate(Double salesCount, Double dollars) {
        return Math.MAX(RateBySales(salesCount), RateByDollars(dollars));
    }

    /**
     * Get the commission rate by number of Sales
     * @param  salesCount Number of monthly sales
     * @return Double     Commission Percentage
     */
    @Testvisible
    private static Double RateBySales(Double salesCount) {
        List<Commission__mdt> commisionRate = [SELECT Monthly_Sales__c, Rate__c
                                               FROM Commission__mdt
                                               WHERE (Monthly_Sales__c <= :salesCount) AND (Test_Data__c = :Test.isRunningTest())
                                               ORDER BY Rate__c Desc
                                               LIMIT 1];

        if (commisionRate.size() == 0) {
            return 0;
        }
        return commisionRate[0].Rate__c;
    }

    /**
     * Get the commission rate by dollar amount sold
     * @param  dollars Dollar value of sales
     * @return Double Commission Percentage
     */
    @Testvisible
    private static Double RateByDollars(Double dollars) {
        List<Commission__mdt> commisionRate = [SELECT Dollar_Amount__c, Rate__c
                                               FROM Commission__mdt
                                               WHERE (Dollar_Amount__c <= :dollars) AND (Test_Data__c = :Test.isRunningTest())
                                               ORDER BY Rate__c DESC
                                               LIMIT 1];

        if (commisionRate.size() == 0) {
            return 0;
        }
        return commisionRate[0].Rate__c;
    }

    /**
     * Method to query all Commission custom meta-data type records, sort by rate
     * @return  List<Commission__mdt>   lstCommissions  list of Commission__mdt records
     */
    public static List<Commission__mdt> getAllCommisions() {
        List<Commission__mdt> lstCommissions = [SELECT Rate__c,
                                                Monthly_Sales__c,
                                                Dollar_Amount__c,
                                                DeveloperName
                                                FROM Commission__mdt
                                                WHERE (Test_Data__c = :Test.isRunningTest())
                                                ORDER BY Rate__c ASC];

        return lstCommissions;
    }

}